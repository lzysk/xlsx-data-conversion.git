# 将 JSON 数据转换为 Excel 表格的详细步骤如下：

工作中 后端 那为兄弟 用java把execl的数据转为json格式发我了,他问我能不能用js把josn在转为execl格式 然后 我就开始了

1. 下载 `xlsx` 库 在使用 `xlsx` 库之前，需要先将其下载到本地。可以使用 npm 命令进行安装：

```md
npm install xlsx
```

​	创建一个 .js文件夹


![输入图片说明](public/publicimage-20230331172120377-1680272690628-1.png)
2. 引入`xlsx`库

```js
let  XLSX = require('xlsx')
```

3. 准备 JSON 数据(数组 或者对象都可以)

```js
let jsonData=[
{ "name": "张三", "age": 25, "gender": "男" },
]

//或者

let  jsonData = {
  "name": "张三",
  "age": 25,
  "gender": "男"
}

```

4. 使用 `xlsx` 库的 `utils` 对象中的方法创建工作簿和工作表。例如，创建一个名为 `Sheet1` 的工作表：

```js
let worksheet = XLSX.utils.json_to_sheet(jsonData)
```

5. 使用 `xlsx` 库的 `utils.book_append_sheet` 方法将工作表添加到工作簿中。例如，将名为 `Sheet1` 的工作表添加到工作簿中：

```js
XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1')
```

6. 使用 `xlsx` 库的 `writeFile` 方法将工作簿保存为 Excel 文件。例如，将工作簿保存为名为 `data.xlsx` 的文件：

```js
XLSX.writeFile(workbook, 'data.xlsx')
```

完整代码

```js
const XLSX = require('xlsx')
// 准备 JSON 数据
const jsonData = [
 省略我的上万条数据
]
// 创建工作簿
const workbook = XLSX.utils.book_new()
// 创建工作表
const worksheet = XLSX.utils.json_to_sheet(jsonData)
// 将工作表添加到工作簿中
XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1')
// 导出 Excel 文件
XLSX.writeFile(workbook, 'data.xlsx')
```



最后执行  =>代码中使用的是 Node.js 环境

![输入图片说明](public/publicimage-20230331172740386.png)

会在当前目录下生成一个名为 `data.xlsx` 的 Excel 文件。

![输入图片说明](public/publicimage-20230331172930062.png)