// execl转json
const XLSX = require('xlsx')
const fs = require('fs')
// 读取 Excel 文件
const workbook = XLSX.readFile('data.xlsx')
// 获取第一个工作表
const worksheet = workbook.Sheets[workbook.SheetNames[0]]
// 将工作表转换为 JSON 数据
const jsonData = XLSX.utils.sheet_to_json(worksheet)
// 导出 JSON 文件
fs.writeFile('data.json', JSON.stringify(jsonData), (err) => {
	if (err) throw err
	console.log('JSON data is saved.')
})